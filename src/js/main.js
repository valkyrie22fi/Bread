$(document).ready(function(){
    // карта
    ymaps.ready(function () {
        var points = [
            [55.7620540, 37.6208420],
            [55.7770219, 37.5814844]
        ];

        var myMap = new ymaps.Map('map', {
            center: [55.762054, 37.620842],
            zoom: 15,
            controls: []
        }, {
            searchControlProvider: 'yandex#search'
        });

        $('#metro').on('change', function() {
            var selected = $('#metro option:selected').val();
            var point = points[selected];
            var myPlacemark = new ymaps.Placemark(point, {
                hintContent: 'Пункт самовывоза'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: 'img/maps/icons/point.png',
                // Размеры метки.
                iconImageSize: [36, 45]
            });
            myMap.geoObjects.removeAll();
            myMap.geoObjects.add(myPlacemark);
            myMap.setCenter(point);
        });
        $('#metro').change();
    });


    // Самовывоз, вывод карты
    $('.group_radio input[type="radio"]').on('change', function() {
        var selectedItem = $('.group_radio input[type="radio"]:checked');
        var total = $('#total').data('amount');
        var deliveryPrice = selectedItem.data('price');
        var deliveryTitle = selectedItem.data('title');
        $('.pick-up_container').removeClass('visible');
        if (selectedItem.val() == 3) {
            $('.pick-up_container').addClass('visible');
        }
        $('#deliveryTitle').text(deliveryTitle);
        $('#deliveryPrice').text(deliveryPrice);
        $('#totalPrice').text(total + deliveryPrice);
    });
    $('.group_radio input[type="radio"]:first').change();

    function strip(html) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    }

    $('#orderForm').on('submit', function() {
        var commentTextArea = $('#commentText');
        var text = commentTextArea.val();
        text = strip(text);
        commentTextArea.val(text);
        return true;
    });

    $('#metro').on('change', function() {
        var selectedOption = $('#metro option:selected');
        var mapAddress = selectedOption.data('address');
        var mapTime = selectedOption.data('time');
        $('#mapAddress').text(mapAddress);
        $('#mapTime').text(mapTime);
    });
});