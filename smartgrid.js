var smartgrid = require('smart-grid');

var settings = {
    outputStyle: 'scss', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '0px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    oldSizeStyle: false,
    container: {
        maxWidth: '1920px', /* max-width оn very large screen */
        fields: '445px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1440px',
            fields: '30px'
        },
        md: {
            width: '1360px',
            fields: '30px'
        },
        sm: {
            width: '768px',
            fields: '10px'
        }
    }
};

smartgrid('./src/scss', settings);